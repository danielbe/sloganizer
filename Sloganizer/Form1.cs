﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sloganizer
{
    public partial class Form1 : Form
    {
        List<string> slogans1 = new List<string>();
        List<string> slogans2 = new List<string>();
        Random random = new Random();

        public Form1()
        {
            InitializeComponent();
            LoadSlogansFromFile();
            LabelInfo.Text = "Dateien müssen im gleichen Verzeichnis liegen, " + Environment.NewLine +
                " wie dieses Programm." + Environment.NewLine + "* als Kennzeichen bei 1 Slogan, " + Environment.NewLine + 
                "* und ~ bei 2 Slogans";
        }

        private void go_Click(object sender, EventArgs e)
        {
            string slogan = GetSlogan(word.Text);
            output.Text += slogan + Environment.NewLine;
        }

        private void go2_Click(object sender, EventArgs e)
        {
            string slogan = GetSlogan(word1.Text, word2.Text);
            output2.Text += slogan + Environment.NewLine;
        }

        private string GetSlogan(string wort)
        {
            return slogans1[random.Next(slogans1.Count)].Replace("*", wort);
        }

        private string GetSlogan(string wort1, string wort2)
        {
            return slogans2[random.Next(slogans2.Count)].Replace("*", wort1).Replace("~", wort2);
        }

        private void tabPage2_Leave(object sender, EventArgs e)
        {
            LoadSlogansFromFile();
        }

        private void LoadSlogansFromFile()
        {
            slogans1.Clear();
            slogans2.Clear();

            try
            {
                slogans1.AddRange(File.ReadAllLines(pathOne.Text, Encoding.GetEncoding("iso-8859-1")));
                slogans2.AddRange(File.ReadAllLines(pathTwo.Text, Encoding.GetEncoding("iso-8859-1")));
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error loading slogans from file");
            }
        }
    }
}
